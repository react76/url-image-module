from typing import Callable, Dict, List, Optional, Set, Tuple, Union
from itertools import combinations
import json
import pandas as pd
import requests
from scipy.stats import norm

def get_label_numbers(data: pd.DataFrame) -> Dict[str, int]:
    """Returns a dictionary with the number of times each label has been selected for each task (e.g. how many values there are per label per task). 
    
    Args:
        data: the DataFrame that contains the annotated data.
            e.g. imgName | authorId | taskName | labelName
    """
    label_numbers = {}

    for row in data.iterrows():
        imgName = row[1][0]
        taskName = row[1][1]
        labelName = row[1][2]
        authorId = row[1][3]

        if taskName not in label_numbers:
            label_numbers[taskName] = {}

        if labelName not in label_numbers[taskName]:
            label_numbers[taskName][labelName] = 0
        else:
            label_numbers[taskName][labelName] += 1
    
    return label_numbers

def get_label_frequencies(data: pd.DataFrame) -> Dict[str, float]:
    """Returns a dictionary with the relative frequency each label has been selected for each task. 
    
    Args:
        data: the DataFrame that contains the data.
            e.g. imgName | authorId | taskName | labelName
    """
    label_numbers = get_label_numbers(data)
    
    label_frequencies = {}

    for task in label_numbers:
        label_frequencies[task] = {}
        task_total = 0
        for label in label_numbers[task]:
            task_total += label_numbers[task][label]
        for label in label_numbers[task]:
            label_frequency = label_numbers[task][label]/task_total
            label_frequencies[task][label] = label_frequency
        label_frequencies[task]['task total'] = task_total
    
    return label_frequencies

def get_pairable_values_per_label(data: pd.DataFrame) -> Dict[str, int]:
    """Returns a dictionary with the number of pairable values per label per task.
    
    Args: 
        data: the DataFrame that contains the data.
            e.g. imgName | authorId | taskName | labelName
        
    """
    api_base_url = "https://react-api.staging.riskmap.org"
    get_all_tasks_url = api_base_url + "/tasks"
    task_res = requests.get(get_all_tasks_url)
    task_json_list = task_res.json()
    
    all_task_labels_map = {task_dict['name']: list(map(lambda label_dict: label_dict['name'], task_dict['labels'])) for task_dict in task_json_list}

    img_labels_dict = dict()
    labels_per_image = dict()

    for row in data.iterrows():
        imgName = row[1][0]
        taskName = row[1][1]
        labelName = row[1][2]
        authorId = row[1][3]
        
        if taskName not in img_labels_dict:
            img_labels_dict[taskName] = {}
            labels_per_image[taskName] = {}
        
        if imgName not in img_labels_dict[taskName]:
            img_labels_dict[taskName][imgName] = []
            labels_per_image[taskName][imgName] = 0
        
        img_labels_dict[taskName][imgName].append(labelName)
        labels_per_image[taskName][imgName] += 1
        
            
    pairable_values_per_label = dict()
    
    for taskName in all_task_labels_map:
        task_labels = all_task_labels_map[taskName]
    
        pairable_values_per_label[taskName] = {l: 0 for l in task_labels}

        for img_name, img_labels in img_labels_dict[taskName].items():
            for label in task_labels:
                if labels_per_image[taskName][img_name] > 1:
                    pairable_values_per_label[taskName][label] += img_labels.count(label)
    
    
    return pairable_values_per_label


def calculate_T_data(p_value: float, alpha_min: float, P_c: float) -> float:
    """ Returns T_data, the minimum number of values required for the distinction between one value c and values non-c to be informationally adequate. 
        (e.g. the minimum number of values required for the distinction between one label and other labels to be informationally adequate)
        
        To trust the reliability of the data used for drawing conclusions about the research questions posed, reliability data should be representative of the population of units to be coded.
        Probability samples assure that each kind of unit in the population to be studied has an equal chance to be included in the sample of reliability data, with the additional provision that each occurs with adequate frequency.
        
        Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.
        
        
    Args:
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        P_c: the probability of value c (the frequency of a label c). Usually use the frequency of the label that occurs the least.
    """
    z = -norm.ppf(p_value)
    T_data = 2*(z**2)*(((1+alpha_min)*(3-alpha_min)/(4*(1-alpha_min)*P_c*(1-P_c)))-alpha_min)
    
    return T_data

def calculate_i_Data(data: pd.DataFrame, p_value: float, alpha_min: float, selected_task: str) -> float:
    """ Proportion of information about the data. 
    Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.
        
        
    Args:
        data: the DataFrame that contains the data.
            e.g. imgName | authorId | taskName | labelName
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        selected_task: the task's name as a string
    """
    # n is the number of pairable values in the data
    pairable_values = sum(get_pairable_values_per_label(data)[selected_task].values())
    n = pairable_values
    
    label_frequencies = get_label_frequencies(data)[selected_task]
    P_c = min(label_frequencies.values())
    
    T_data = calculate_T_data(p_value, alpha_min, P_c)
    
    
    i_data = min(1, n/T_data)
    return i_data

def calculate_values_to_add_Data(data: pd.DataFrame, p_value: float, alpha_min: float, selected_task: str) -> float:
    """Returns the number of values to add by using the result from i_Data.
    Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.
    
    Args:
        data: the DataFrame containing the annotations 
            eg. imgName | authorId | taskName | labelName
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        selected_task: the task's name as a string
    """
    label_frequencies = get_label_frequencies(data)[selected_task]
    P_c = min(label_frequencies.values())
    
    i_data = calculate_i_Data(p_value, alpha_min, selected_task)
    
    T_data = calculate_T_data(p_value, alpha_min, P_c)
    
    return (1-i_data) * T_data

def calculate_T_coding(p_value: float, alpha_min: float, V: int) -> float:
    """ The reproductibility of a coding. Distinction among units, defined by the values available for coding, is made with adequate frequency.
    An informationally adequate sample must have sufficient diversity to cover all values of a variable. 
    Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.

    Returns T_coding, the minimally required number of values 
    Args:
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        V: the number of values c available for coding the variable in question (how many labels there are)
    """
    z = -norm.ppf(p_value)
    T_coding = 2*(z**2)*(((1+alpha_min)*(3-alpha_min)/(4*(1-alpha_min)*(1/V)*(1-(1/V))))-alpha_min)
    
    return T_coding


def calculate_i_Coding(data: pd.DataFrame, p_value: float, alpha_min: float, selected_task: str) -> float:
    """Proportion of information about all values c in a variable (all labels in a task).
        Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.
        
    Args:
        data: the DataFrame that contains the data.
            e.g. imgName | authorId | taskName | labelName
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        selected_task: the task's name as a string       
    """
    api_base_url = "https://react-api.staging.riskmap.org"
    get_all_tasks_url = api_base_url + "/tasks"
    task_res = requests.get(get_all_tasks_url)
    task_json_list = task_res.json()
    
    all_task_labels_map = {task_dict['name']: list(map(lambda label_dict: label_dict['name'], task_dict['labels'])) for task_dict in task_json_list}
    
    labels = all_task_labels_map[selected_task]
    V = len(labels)
    
    T_coding = calculate_T_coding(p_value, alpha_min, V)
     
    sum_i_coding = 0
    pairable_values = get_pairable_values_per_label(data)[selected_task]
    
    for label in labels:
        n_c = pairable_values[label]
        i_coding_c = min(1, V*n_c/T_coding)
        sum_i_coding += i_coding_c/V
    
    return sum_i_coding

def calculate_values_to_add_Coding(p_value: float, alpha_min: float, selected_task: str) -> float:
    """Returns the number of values to add by using the result from i_Coding.
    Formulas are referenced from https://www.tandfonline.com/doi/full/10.1080/19312458.2011.568376.

    Args:
        p_value: the chosen level of significance.
        alpha_min: the smallest Krippendorff's alpha for data to be accepted as reliable
        selected_task: the task's name as a string
    """
    api_base_url = "https://react-api.staging.riskmap.org"
    get_all_tasks_url = api_base_url + "/tasks"
    task_res = requests.get(get_all_tasks_url)
    task_json_list = task_res.json()
    
    all_task_labels_map = {task_dict['name']: list(map(lambda label_dict: label_dict['name'], task_dict['labels'])) for task_dict in task_json_list}
    
    labels = all_task_labels_map[selected_task]
    V = len(labels)
    
    i_Coding = calculate_i_Coding(p_value, alpha_min, selected_task)
    
    return (1-i_Coding) * calculate_T_coding(p_value, alpha_min, V)
