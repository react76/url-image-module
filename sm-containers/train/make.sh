# setup aws credentials file by putting a profile in ~/.aws/credentials
# [riskmap-japan]
# aws_access_key_id=...
# aws_secret_access_key=...

# then intall the aws cli
# finally log in to be able to pull the base image, then build from it
AWS_PROFILE=riskmap-japan aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 763104351884.dkr.ecr.us-east-1.amazonaws.com

GIT_SHORT_HASH=$(git rev-parse --short HEAD)
sh -ac ' . ../../.env; docker build --build-arg GITLAB_TOKEN_USER=$GITLAB_TOKEN_USER --build-arg GITLAB_TOKEN=$GITLAB_TOKEN -t riskmap-japan-private:$GIT_SHORT_HASH .'

# now push to the urbanrisklab Elastic Container Registry (ECR)
# login to the URL ECR, different from where the base image is stored
AWS_PROFILE=riskmap-japan aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 964559602764.dkr.ecr.us-east-1.amazonaws.com

docker tag riskmap-japan-private:$GIT_SHORT_HASH 964559602764.dkr.ecr.us-east-1.amazonaws.com/riskmap-japan-private:$GIT_SHORT_HASH

docker push 964559602764.dkr.ecr.us-east-1.amazonaws.com/riskmap-japan-private:$GIT_SHORT_HASH
